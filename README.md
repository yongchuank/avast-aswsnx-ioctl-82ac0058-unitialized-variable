Advisory of Avast Ticket-ID XPB-908-54508:
- Avast AntiVirus (aswSnx.sys) IOCTL 0x82AC0054/0x82AC0058 Unitialized Variable
- No CVE is assigned

Timeline:
- 2019-08-01 Reported vulnerability and POC to Avast
- 2019-08-16 Avast acknowledged report and assigned ticket-ID XPB-908-54508
- 2019-08-16 Avast replied that this vulnerability will be fixed in v19.8